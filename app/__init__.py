import pandas as pd

from introduccion import region_request
from regiones import regiones
from paises import pais, pais_region
from idiomas import idioma, idioma_pais, encrip

def tabla_paises():
    tabla = pd.DataFrame()


    tabla['Regiones'] = regiones()
    tabla['Paises'] = pais_region()
    tabla['Idioma'] = encrip()

    tabla.to_json("data.json") 
    
    return tabla

