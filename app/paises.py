import random
from introduccion import region_request
from regiones import regiones

    
def pais(region):
    data_total = region_request()
    paises=[]
    for i in data_total:
        if (i['region'] == region):
            paises.append(i['name'])
    cualquier_pais=random.randrange(0,len(paises))
    #print(paises)
    return paises[cualquier_pais]

def pais_region():
    region=regiones()
    paises_sel=[]
    for i in range(len(region)):
        paises_sel.append(pais(region[i]))

    #print(paises_sel)
    return paises_sel
    