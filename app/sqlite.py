from sqlalchemy import create_engine
from __init__ import tabla_paises

engine = create_engine('sqlite://', echo=False)

tabla_paises().to_sql('tabla', con=engine)

engine.execute('SELECT* FROM tabla').fetchall() #recuperar los datos
