import hashlib
from introduccion import region_request
from regiones import regiones
from paises import pais, pais_region

def idioma(paises_sel):
    data_total = region_request()
    idiomas=[]
    for i in data_total:
        if (i['name'] == paises_sel):
            idiomas.append(i['languages'][0])
    return idiomas

def idioma_pais():
    idiomas=[]
    for i in range(len(paises_sel)):
        idiomas.append(idioma(paises_sel[i]))
    return idioma_pais


def encrip():
    encrip=[]
    for m in idiomas:
        #print(m[0])
        transf=bytes(m[0],"utf-8")
        h = hashlib.sha1(transf)
        print(h.hexdigest())
        encrip.append(h.hexdigest())
    return encrip