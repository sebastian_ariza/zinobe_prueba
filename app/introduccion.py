import requests
from time import time

def region_request():

    url = "https://restcountries-v1.p.rapidapi.com/all"

    headers = {
    'x-rapidapi-key': "3dfa895c1dmshb76b6d57a0ae7bcp10734djsn058f0edcd026",
    'x-rapidapi-host': "restcountries-v1.p.rapidapi.com"
    }
    

    response = requests.request("GET", url, headers=headers)
    
    #print(response.json())

    return response.json()

region_request()